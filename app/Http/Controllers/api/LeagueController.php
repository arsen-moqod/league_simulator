<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Leagues;
use App\Matches;
use App\Teams;
use Illuminate\Http\Request;

class LeagueController extends Controller
{
    public function index()
    {
        return Leagues::all();
    }

    private function findUniquePairs()
    {
        $teams_count = Teams::all()->count();
        $arr = range(1, $teams_count);
        $alreadyProcessed = array();
        foreach ($arr as $first) {
            foreach ($arr as $second) {
                $combination = array($first, $second);
                if ($first === $second || in_array($combination, $alreadyProcessed)) {
                    continue;
                }
                $alreadyProcessed[] = $combination;
            }
        }
        shuffle($alreadyProcessed);
        return $alreadyProcessed;
    }

    static function matchesPerWeek()
    {
        $divided = Teams::all()->count() / 2;
        return $divided > 5 ? 5 : $divided;

    }

    public function create(Request $request)
    {
        $league = Leagues::create([
            'name' => $request->post('name')
        ]);
        $unique_pairs = $this->findUniquePairs();
        $weeks_count = count($unique_pairs) / self::matchesPerWeek();
        $selected_pairs_for_league = [];
        for ($w = 0; $w < $weeks_count; $w++) {
            $selected_teams_weekly = [];
            foreach ($unique_pairs as $pair) {
                if (in_array(implode('', $pair), $selected_pairs_for_league)) {
                    continue;
                }

                if (!in_array($pair[0], $selected_teams_weekly) && !in_array($pair[1], $selected_teams_weekly)) {
                    $selected_teams_weekly = array_merge($selected_teams_weekly, $pair);
                    Matches::create([
                        'league_id' => $league->id,
                        'week' => $w + 1,
                        'home_team_id' => $pair[0],
                        'guest_team_id' => $pair[1],
                    ]);
                    $selected_pairs_for_league[] = implode('', $pair);
                }
            }
        }
        return [
            'matches' => Matches::where([
                'league_id' => $league->id
            ])->get(),
            'league' => $league
        ];

    }

    public function league(Request $request)
    {
        $league = Leagues::find($request->id);
        $matches = Matches::where([
            'league_id' => $request->id,
            'week' => $request->week,
        ])->with(['homeTeam', 'guestTeam'])->get();
        return [
            'matches' => $matches,
            'league' => $league
        ];

    }

    public function playWeek(Request $request)
    {
        $matches = Matches::with(['homeTeam', 'guestTeam'])->where(['league_id' => $request->id, 'week' => $request->week])->get();
        foreach ($matches as $match) {
            $match->home_goals = rand(0, $match->homeTeam->strength);
            $match->guest_goals = rand(0, $match->guestTeam->strength);
            $match->save();
        }
        return $matches;
    }

    private function standingsReport($league_id)
    {
        $report = [];
        $matches = Matches::where(['league_id' => $league_id])->get();
        $teams = Teams::all();
        foreach ($teams as $team) {
            $team_report = [
                'team_name' => $team->name,
                'played' => 0,
                'won' => 0,
                'drawn' => 0,
                'lost' => 0,
                'gf' => 0,
                'ga' => 0,
                'gd' => 0,
                'points' => 0,
            ];
            foreach ($matches as $match) {
                if ($match->home_goals === null) {
                    continue;
                }
                $is_home = $match->home_team_id === $team->id;
                $is_guest = $match->guest_team_id === $team->id;
                if ($is_home || $is_guest) {
                    if ($match->home_goals === $match->guest_goals) {
                        $team_report['drawn']++;
                        $team_report['points']++;
                    }
                    $team_report['played']++;

                }
                if ($is_home) {
                    $team_report['gf'] += $match->home_goals;
                    $team_report['ga'] += $match->guest_goals;
                    $team_report['gd'] += $match->home_goals;
                    $team_report['gd'] -= $match->guest_goals;
                    if ($match->home_goals > $match->guest_goals) {
                        $team_report['won']++;
                        $team_report['points'] += 3;
                    } elseif ($match->home_goals < $match->guest_goals) {
                        $team_report['lost']++;
                    }
                }
                if ($is_guest) {
                    $team_report['gf'] += $match->guest_goals;
                    $team_report['ga'] += $match->home_goals;
                    $team_report['gd'] += $match->guest_goals;
                    $team_report['gd'] -= $match->home_goals;
                    if ($match->guest_goals > $match->home_goals) {
                        $team_report['won']++;
                        $team_report['points'] += 3;
                    } elseif ($match->guest_team_id < $match->home_goals) {
                        $team_report['lost']++;
                    }
                }
            }
            $report[] = $team_report;
        }
        usort($report, function ($a, $b) {
            return $a['gd'] < $b['gd'];
        });
        usort($report, function ($a, $b) {
            return $a['points'] < $b['points'];
        });
        return $report;
    }

    public function standings(Request $request)
    {
        return $this->standingsReport($request->id);
    }

    public function editWeek(Request $request)
    {
        foreach ($request->all() as $match) {
            Matches::find($match['id'])->update([
                'home_goals' => $match['home_goals'],
                'guest_goals' => $match['guest_goals'],
            ]);
        }
    }

    public function predictions(Request $request)
    {
        $predictions = [];
        $standings = $this->standingsReport($request->id);
        $points_sum = 0;
        foreach ($standings as $stand) {
            $points_sum += $stand['points'];
        }
        foreach ($standings as $standing) {
            $predictions[] = [
                'team_name' => $standing['team_name'],
                'percentage' => round($standing['points'] / $points_sum * 100, 1)
            ];
        }
        return $predictions;
    }
}

