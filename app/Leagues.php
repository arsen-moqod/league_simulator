<?php

namespace App;

use App\Models\CNC\Part;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Leagues extends Model
{
    protected $fillable = [
        'name'
    ];

    public function matches() : HasMany
    {
        return $this->hasMany(Matches::class);
    }
}
