<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Matches extends Model
{
    protected $fillable = [
        'league_id', 'home_team_id', 'guest_team_id', 'week', 'home_goals', 'guest_goals'
    ];

    public function league() : BelongsTo
    {
        return $this->belongsTo(Leagues::class);
    }
    public function homeTeam() : HasOne
    {
        return $this->hasOne(Teams::class,'id','home_team_id');
    }
    public function guestTeam() : HasOne
    {
        return $this->hasOne(Teams::class,'id','guest_team_id');
    }

}
