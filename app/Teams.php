<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    protected $fillable = [
        'name', 'strength'
    ];

    public static function getRandomExcept($except)
    {
        return self::where('id', '!=', $except)->inRandomOrder()->first();
    }

    public static function getRandom()
    {
        return self::whele([])->inRandomOrder()->first();
    }
}
