<?php

use Illuminate\Database\Seeder;

class TeamsTableDataSeeder extends Seeder
{
    public $teams = ['Liverpool','Manchester City','Chelsea','Manchester United'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 4; $i++) {
	    	\App\Teams::create([
	            'name' => $this->teams[$i],
	            'strength' => rand(1,5),
	        ]);
    	}
    }
}
