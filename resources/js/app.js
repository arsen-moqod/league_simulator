import Vue from 'vue'
import VueRouter from 'vue-router'
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'



Vue.use(VueRouter)
Vue.use(ElementUI, { locale })

import App from '../views/App'
import NewLeague from '../components/NewLeague'
import Leagues from '../components/Leagues'
import League from '../components/League'


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/new_league',
            name: 'new_league',
            component: NewLeague
        },
        {
            path: '/',
            name: 'leagues',
            component: Leagues
        },
        {
            path: '/league/:id/:week',
            name: 'league',
            component: League
        },
        {
            path: '/league/:id',
            name: 'league',
            component: League
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
