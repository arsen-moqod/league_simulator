<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['api'])->get('/leagues', 'api\LeagueController@index');
Route::middleware(['api'])->post('/leagues/create', 'api\LeagueController@create');
Route::middleware(['api'])->get('/league/{id}/standings', 'api\LeagueController@standings');
Route::middleware(['api'])->get('/league/{id}/predictions', 'api\LeagueController@predictions');
Route::middleware(['api'])->get('/league/{id}/{week}', 'api\LeagueController@league');
Route::middleware(['api'])->post('/league/{id}/play/{week}', 'api\LeagueController@playWeek');
Route::middleware(['api'])->post('/league/{id}/edit/{week}', 'api\LeagueController@editWeek');
