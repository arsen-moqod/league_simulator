const mix = require('laravel-mix');

require('laravel-mix-tailwind');

function resolve(dir) {
    return path.join(__dirname, '..', dir)
}

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
module.exports = {
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            '@': path.resolve(__dirname, 'resources'),
            'vendor': resolve('vendor')
        },
    },
}

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css')
    .tailwind('./tailwind.config.js');

if (mix.inProduction()) {
    mix
        .version();
}

